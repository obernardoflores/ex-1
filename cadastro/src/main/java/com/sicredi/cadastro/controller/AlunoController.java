package com.sicredi.cadastro.controller;

import com.sicredi.cadastro.model.Aluno;
import com.sicredi.cadastro.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/aluno")
public class AlunoController {
    @Autowired
    private AlunoService alunoService;
    @GetMapping
    public List<Aluno> listAllAlunos(){
        return alunoService.listAllAlunos();
    }
    @GetMapping("/{id}")
    public Aluno listAlunoID(@PathVariable Long id){
        return alunoService.ListAlunoId(id);
    }
    @PostMapping
    public ResponseEntity<Aluno> addAluno(@RequestBody Aluno aluno){
        Aluno alunoAdd = alunoService.addAluno(aluno);
        return ResponseEntity.status(HttpStatus.CREATED).body(aluno);
    }
    @PutMapping("/{id_aluno}/{id_disciplina}")
    public  ResponseEntity<Aluno> changeDisciplina(@PathVariable Long id_aluno, @PathVariable Long id_disciplina){
        return ResponseEntity.ok(alunoService.changeDisciplina(id_aluno,id_disciplina));
    }
    @PutMapping("/{id}")
    public ResponseEntity<Aluno> changeAluno(@PathVariable Long id, @RequestBody Aluno aluno){
        return ResponseEntity.ok(alunoService.changeAluno(id, aluno));
    }
    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAluno (@RequestBody Aluno aluno){
        Long id = aluno.getId();
        alunoService.deleteAluno(id);
    }
}
