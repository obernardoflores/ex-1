package com.sicredi.cadastro.controller;

import com.sicredi.cadastro.model.Disciplina;
import com.sicredi.cadastro.service.DisciplinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/disciplina")
public class DisciplinaController {
    @Autowired
    private DisciplinaService disciplinaService;
    @GetMapping
    public List<Disciplina> listAll(){
        return disciplinaService.listAll();
    }
    @PostMapping
    public ResponseEntity<Disciplina> add(@RequestBody Disciplina disciplina){
        Disciplina disciplinaAdd = disciplinaService.add(disciplina);
        return ResponseEntity.status(HttpStatus.CREATED).body(disciplinaAdd);
    }
    @PutMapping("/{id}")
    public ResponseEntity<Disciplina> change(@PathVariable Long id, @RequestBody Disciplina disciplina){
        return ResponseEntity.ok(disciplinaService.change(id, disciplina));
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        disciplinaService.delete(id);
    }
}
