package com.sicredi.cadastro.repository;

import com.sicredi.cadastro.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlunoRepository extends JpaRepository<Aluno, Long> {
}
