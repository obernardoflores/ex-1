package com.sicredi.cadastro.repository;

import com.sicredi.cadastro.model.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisciplinaRepository extends JpaRepository<Disciplina, Long> {
}
