package com.sicredi.cadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.Entity;

@SpringBootApplication
@EntityScan(basePackages = "com.sicredi.cadastro.model")
@EnableJpaRepositories(basePackages = "com.sicredi.cadastro.repository")
@ComponentScan(basePackages = {"com.sicredi.cadastro.controller", "com.sicredi.cadastro.service"})
public class CadastroApplication {
	public static void main(String[] args) {
		SpringApplication.run(CadastroApplication.class, args);
	}

}
