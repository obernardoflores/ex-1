package com.sicredi.cadastro.service;

import com.sicredi.cadastro.model.Aluno;
import com.sicredi.cadastro.model.Disciplina;
import com.sicredi.cadastro.repository.AlunoRepository;
import com.sicredi.cadastro.repository.DisciplinaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class AlunoService {
    @Autowired
    private AlunoRepository alunoRepository;
    @Autowired
    private DisciplinaRepository disciplinaRepository;
    public Aluno addAluno(Aluno aluno){
        return alunoRepository.save(aluno);
    }

    public void deleteAluno(Long id){
        alunoRepository.deleteById(id);
    }
    public List<Aluno> listAllAlunos(){
        return alunoRepository.findAll();
    }

    public Aluno ListAlunoId(Long id){
        Optional<Aluno> aluno = alunoRepository.findById(id);
        if (aluno.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return aluno.get();
    }

    public Aluno changeDisciplina(Long id_aluno, Long id_disciplina){
        Aluno alunoChanged = validateAluno(id_aluno);
        Disciplina disciplina = validateDisciplina(id_disciplina);
        alunoChanged.getDisciplinas().add(disciplina);
        return alunoRepository.save(alunoChanged);
    }

    public Disciplina validateDisciplina(Long id){
        Optional<Disciplina> disciplina = disciplinaRepository.findById(id);
        if (disciplina.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return disciplina.get();
    }
    public Aluno changeAluno(Long id, Aluno aluno){
        Aluno alunoChanged = validateAluno(id);
        BeanUtils.copyProperties(aluno, alunoChanged, "id");
        return alunoRepository.save(alunoChanged);
    }
    public Aluno validateAluno(Long id){
        Optional<Aluno> aluno = alunoRepository.findById(id);
        if(aluno.isEmpty()){
            throw new EmptyResultDataAccessException(0);
        }
        return aluno.get();
    }

}
