package com.sicredi.cadastro.service;

import com.sicredi.cadastro.model.Disciplina;
import com.sicredi.cadastro.repository.DisciplinaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DisciplinaService {
    @Autowired
    private DisciplinaRepository disciplinaRepository;
    public Disciplina add(Disciplina disciplina){
        return disciplinaRepository.save(disciplina);
    }
    public List<Disciplina> listAll(){
        return disciplinaRepository.findAll();
    }
    public void delete(Long id){
        disciplinaRepository.deleteById(id);
    }
    public Disciplina change(Long id, Disciplina disciplina){
        Disciplina disciplinaChange = validateDisciplina(id);
        BeanUtils.copyProperties(disciplina, disciplinaChange, "id");
        return disciplinaRepository.save(disciplinaChange);
    }
    public Disciplina validateDisciplina(Long id){
        Optional<Disciplina> disciplina = disciplinaRepository.findById(id);
        if(disciplina.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return disciplina.get();
    }
}
